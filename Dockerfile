FROM node:8.17-alpine

# Harmonise www-data uid with Ubuntu, as we mount storage from Ubuntu inside
RUN deluser xfs \
    && adduser -D -u 33 -s /bin/false www-data

# Web application
COPY --chown=www-data ./build/etherpad /app

# Get Etherpad-lite's other dependencies
RUN apk update && apk add --no-cache \ 
  #abiword \
  #abiword-plugins \
  curl \
  gettext \
  gzip \
  openssl-dev \
  pkgconfig \
  python

# Scripts
COPY --chown=www-data ./scripts/run.sh /scripts/run.sh
RUN chmod -R 755 /scripts

USER www-data

# Install node dependencies
RUN /app/bin/installDeps.sh

# Bug fix https://github.com/ether/etherpad-lite/issues/3302
RUN rm /app/src/package-lock.json

# Add conf files
ADD config/settings.json app/settings.json.template
ADD config/APIKEY.txt app/APIKEY.txt.template

EXPOSE 9001
CMD ["/scripts/run.sh"]
