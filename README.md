# Docker build scripts for Etherpad

First fetch all requirements with

    ./prepare-build.sh

then build the container using

    docker build -t pleio/etherpad .

Then boot the container with

        docker run -d -p 9001:9001 -t pleio/etherpad