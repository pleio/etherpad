#!/bin/sh

echo "[i] Configuring settings..."
envsubst < /app/settings.json.template > /app/settings.json
envsubst < /app/APIKEY.txt.template > /app/APIKEY.txt

/app/bin/run.sh
